### Experimentation with cloudy substances

Tests found the system to highly responsive to minute particulates in the water.  The findings are as below.

#### Milo

Our first experiments involved cloudy substances.  They were chosen, as a sensible representative of dirt / mud in water.  One such ingredient of choice was Milo.

We define a `nibble` as the quantity of Milo given in the picture below:

![nibble](images/2014-04-02 15.56.51.jpg)

Milo was found to be irresponsive after 3 nibbles.  The cloudiness consistency, stopping bepping, is given by the following picture:

![](images/2014-04-02 15.59.14.jpg)

We believe the cloudiness is attributed to the presence of dried milk particles within the Milo packet.  These particles sufficiently reflected and blocked the infrared transmission of light.

It is also noted that that subsequent readings reached a voltage minimum, before steadily climbing to an indeterminate value.  We believe that this climb is attributed to the suspension of particles blocking the light medium on the occassion their paths crossed.


#### Flour

Flour was found to have a slightly more tolerant input.  Flour was chosen as it created a white suspension that might be expected to have better performance.

In our initial experiment with flour, the system was found to be extremely responsive to be measured quantatively.  To slow down the process of change, a more diluted sample was consistently added to our test sample of water.  The diluted flour suspension added had a concentration of 1 teaspoon of flour, in 1 cup of water.

![](plots/Flour.png)

However, the same readings persisted.  We found that the signal had completely died off (buzzer stopped beeping) after 2ml of the liquid was added.  A significant drop in the signal (buzzer amplitude) was even detectable after just 1ml of diluted liquid added to the pure water sample.

Hence, our tests show that the experiments are highly responsive to the addition of cloudy suspensions.  This might suggest its reponsiveness to detect mud / presence of minute microorganisms in water.

![cornflour](images/2014-04-08 11.10.37.jpg)

