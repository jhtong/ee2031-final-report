###Signal Processing Characterization

The experimental properties of the signal processing circuit were verified through a LTSpice model. This simulation served as a means of validating the desired functionality of the device.

##### Simulated Characteristics

The results of our model (see 'Circuit Diagram' section for diagram):

![](plots/Simulated Signal Amplification.png)

Simulated Gain:

$$
A\_{v, simulation} = \frac{V\_{output}}{V\_{input}} =  -3.78424
$$

In the simulation, the IR receiver was replaced by its Thevenin equivalent circuit, i.e. a varying voltage source and a resistor. Note: this is not a completely accurate model. The DC sweep range of this source was set to: 0.013V - 4.6V (at 0.01 increments). This range was determined empirically using the actual prototype.

#####Experimental Characteristics

![](plots/Signal Amplification.png)

Real Gain:

$$
A\_{v, real} = \frac{V\_{output}}{V\_{input}} =  -3.47956
$$

Percentage Error = 8.05%. Although the simulated gain and the real gain are reasonably close to each other, the scales of the two characterizations are completely off. This is possibly because of the difference in the properties of the 'universal opamp' used in the simulation and the LM358 used in the prototype. Plus, the internal resistance of the voltage source (used for the experiment) wasn't considered in this analysis.