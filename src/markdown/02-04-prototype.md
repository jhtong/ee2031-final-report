#### Final prototype

Our final working prototype is given by the picture below.  It was found to be highly accurate in determining minute impure substances in water.  Some particles were even almost undetectable by the human eye!

Our set-up is given by the picture shown below.  A distance of 9.5cm in water was chosen, as that was the length of the container, obtained from Daiso.  The container was chosen to be of translucent material, to preserve readings.  The light transmitter and receiver must sit snugly within the container.

![](images/box.jpg)
