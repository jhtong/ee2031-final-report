## Problem

We wish to determine the purity of water in a given sample. We foresee a myriad of potential applications and scenarios where this ability would be highly valuable. When we refer to measuring 'purity', we are alluding to the ability to distinguish between clear water and water adulterated with moderately large impurities. We understand that judging the drinkability of water is another challenge in itself, so we are more focused on the areas of implementation which require the automation of detecting sizable particles or viscous liquids. The purity of a liquid is often determined through the use of slow, cumbersome processes. Some of these processes are listed below:

### Boiling water

The presence of minerals in water would cause changes in the boiling point and melting point of the liquid.  Hence inpure substances are unlikely to have a boiling point of exactly 100 degrees Celsius.  This involves waiting and boiling water - both of which are slow and waste energy.


### Testing for electrical conductivity

Pure water is non-conducting.  Certain impure substances, such as salt, make water more conductive.  The detection of higher conductivity might hence imply the presence of particles in water.

However, this does not necessarily happen for all impure substances.  Furthermore, electrolysis is an invasive process and renders the test water toxic (due to the presence of metal ions after testing).


### So what if...

We could instantly determine if water is pure, cheaply, effectively - without wasting water?

### Potential Applications:

The prospective applications of our device vary in a wide range of contexts.

**Water pipelines:**

Although our sensor is unable to determine the absolute drinkability of water in supply pipelines, it could still act as the first line of defense in detecting major contaminations caused leaks. Furthermore, our device could also be useful in detecting rust particulates in faucets before they reach a hazardous level.


**Quality control:**

Our sensor is able to detect minute particles within liquids.  This may be useful for companies exporting liquid subsances, for instance, ensuring that extra-virgin olive oil (the highest grade of olive oil) is free from olive seed particles during the pressing and refining process.


**Marine exploration:**

Underwater submersibles are often used to study the presence of organisms such as plankton, which are considered to be crucial for marine life food chains. Our device could serve as a low-cost sensor to detect large clouds of planktons (at high speed) before more comprehensive analyses can be carried out using proper equipment.

**Viscous Liquid Characterization:**

From our experimental observations, we have found that our product is reasonably good at characterizing different liquids when added in known amounts to clear water. This could serve as a quick & cheap way of identifying various liquids. Areas of application include: biochemistry, forensic science etc.

**Fish Tank:**

A rather peculiar application, but filters used for cleaning fish tanks might hugely benefit from live 'murkiness' readings to control the active feedback system.

**Cooking:**

Could be used to accurately measure ingredients mixed in liquids, to standardize cooking procedures.

