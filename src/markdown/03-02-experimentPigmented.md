### Experimentation with pigmented liquids

The previous section dealt with mixing large particles in water. Now we look at how dissolving heavily pigmented liquids affect the circuit response.

#### Cola

![](plots/Cola.png)

The dark, carbonated liquid produced a linear reponse when quantized amounts of the drink were steadily added to clear water. To the visible eye, cola seems like an opaque liquid. But under the vision of a near-infrared camera, the substance becomes translucent:

![](images/coke IR.png "Cola in near-IR vision" "float:right;")

Nevertheless, the liquid is still distinct in the IR spectrum; cola absorbs IR radiation. And our experimental results concur with this phenomenon. As more amounts of the liquid were added, more radiation from the transmitter was intercepted by the solution. It's also important to note that the fizzy nature of the liquid has an effect on the circuit reponse (mildly). But we will deal with this in the next section.

Although we haven't experimented with a wide range of colors, it's reasonable to assume from this test that the absorbption of visible light has very little effect on the response. This is because the device mainly deals with IR spectroscopy, so technically the visible color of the liquid shouldn't have any direct effect on the output.


#### Soya Sauce

![](plots/Soya Sauce.png)

From the graph above, this everyday condiment seems to have exploited the entire output range of our signal processing circuit. Ignoring the clippings at the end, the correlation seems to be mostly linear. This occurrence is quite similar to that of cola & water. The only difference seems to be the stability of the linear gain. It might be that when IR radiation travels through carbonated cola, it gets inconsistently refracted by the erratic nature of the fizzy liquid. On the other hand, soya sauce is much more 'still' when compared to cola. So it's more likely to produce stable results. However, this experiment wasn't repeated to check for consistency, so it might as well be a fluke.


Pigmented liquids are perhaps the most interesting materials we have tested, because they highlight one of the unique features about our product. That is the ability to discern particles, which might not be apparent in the visible spectrum. Imagine if there is a need to detect large particles inside dark liquids. Normal cameras or LDRs might not be useful. We know from the previous section that our device is very sensitive to large particles. And now we know that it can also see through seemingly opaque liquids. So by our observations this device should perform very well under such needs.






