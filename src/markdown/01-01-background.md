## Our Solution and Theory

We propose a cost-effective solution to the problem at hand.  We argue that by directing an infrared light source through the container and collecting its response on the other, we can effectively and instantly detect the presence of many impure particles in water, through the use of infrared absoption.  Our solution was found to dectect pure from impure water readily - however minute - and in some cases, even detect the quantity of impurities within the liquid itself.  Our method is similar to infrared spectroscopy.


### Theory

Water is a special substance.  It is simiilar to a band-pass filter for light, allowing light within the visible spectra of 400nm - 700nm to pass through, and blocking other portions of light.  It is also able to allow the lower spectrum of infrared through itself.  The figure below shows this in action:

![](images/light_absorption.jpg)


Our surroundings do not appear to emit a sizeable amount of infrared light, as compared to the visible spectrum.  Therefore, by shining infrared light through water, most of the infrared light should be readily obtained.  An absence of infrared light would imply impure water, due to the absorption and "diffuse-scattering" of light particles.

