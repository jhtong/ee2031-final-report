### Implementing the Transform Function

The transform function was implemented by a voltage differentiating amplifier.  The circuit design is able to utilize a single amplifier for voltage subtraction, and then amplification.  The equation is given below, with $$$R_1 = R_2$$$ and $$$R_3 = R_4$$$:

$$
V\_{out} = \frac {R_3} {R_1}(V_2 - V_1)
$$

Hence, the larger the gain $$$G = \frac {R_3} {R_1}$$$, the more sudden the change (similar to a digital output).  To detect gradual changes instead, we chose a gain of 4.  This led to values of $$$R_1 = 1k \Omega$$$, and $$$R_3 = 3.85k \Omega$$$.  The value of $$$R_1$$$ was chosen to yield varying gains during the experimentation process without replacing $$$R_1$$$ and $$$R_2$$$.

In our implementation of Sniffy, $$$V\_2 = V\_\text{SENSOR OUTPUT}$$$ and $$$V_1 = \frac{R_6}{R_5 + R_6} = 0.5357V$$$.  As changes in voltage level $$$V_2$$$ were highly sensitive to relatively minute changes in $$$V_1$$$, $$$V_1$$$ was experimentally determined by a potentiometer, before being fixed by resistors $$$R_5$$$ and $$$R_6$$$ that closely corresponded to the gain.  Similar to the sensor input, $$$V_2$$$ was put through a unity-gain decoupling op-amp buffer as well, to decouple the circuit.

This resulted in the output value given by the reference $$$v\_\text{out}$$$.

