### Experimentation with clear liquids

#### Sprite

![](plots/Sprite.png)

Sprite doesn't seem to be very effective in changing the response of the device. Nonetheless, there is still a noticeable difference between the voltage readings of clear water and that of water mixed with Sprite. The only phenomenon we can deduce/confirm from this experiment is the effect of 'fizzy' liquids, as mentioned in the previous section. Refraction supposedly has a perceptible effect on the amount IR radiation that passes through the solution.