## Circuit Design

Testing for water impurity was achieved with our circuit below.  In our design, we had a set-up that was similar to an opto-coupler.  Our circuit was markedly different from that of an opto-coupler, as it was designed to read analog inputs - opto-couplers are solely used to transmit and de-couple digital data.  Our proposed circuit was found to be highly sensitive to water impurities, and is successful.  The circuit is detailed below:

![](images/ckt-overview.png)

Our proposed circuit can be simplified by the following transfer function block diagram, given below.  It is powered by two voltage sources - a 5V and 15V supply:

$$
\text{INPUT_SIGNAL} \Rightarrow \text{VOLTAGE_SUBTRACTOR} \Rightarrow \text{VOLTAGE_AMPLIFIER} \Rightarrow \text{OUTPUT_SIGNAL}
$$

The input signal is significantly small.  In addition, it carries a certain small amount of offset $$$\Delta a$$$ with it.  This offset has to be removed prior to amplification, for if it is not, the amplified signal $$$v\_{out}$$$ would be wrongly given by:

$$
v\_{out\_{wrong}} = A\_m v\_{raw} = A\_m \Delta a + A\_m v\_i
$$

Where $$$v_{raw}$$$ is the raw voltage input, and $$$A_m$$$ is the amplification of the sensor.  The correct formula would hence be:

$$
v\_{out} = A\_m (v\_{raw} - \Delta a) = A\_m v\_i
$$

As a single op-amp voltage subtractor and voltage amplifier circuit exists (a voltage differentiator), the following transformation is directly simplified into the following transformation: 

$$
\text{INPUT_SIGNAL} \Rightarrow \text{VOLTAGE_DIFFERENTIATOR} \Rightarrow \text{OUTPUT_SIGNAL}
$$
