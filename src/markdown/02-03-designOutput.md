### Formatting the output and load

The output signal from the transform function $$$v\_{out}$$$ was then put through a buzzer.  In our experimentation, we found that the load must be around the range of $$$1M\Omega$$$.  A buzzer was found to be a suitable output for this range.

To limit the current output for safety reasons, a zener diode with a reverse breakdown rating of 10V was used in reverse-bias mode.  This ensured that output was kept at a maximum of 10V, with excess voltage draining to the Zener instead.  This ensured that the buzzer does not burn out.

