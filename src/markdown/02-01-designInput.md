### Generating the input signal

Our design involved connecting a series of Infrared LEDs (TIL83), that were within visible range of an Infrared Receiver (TIL81).  The sensor output signal was referenced by the voltage drop across the TIL81 photodiode.  This schematic is shown in the figure below:

![](images/ckt-input.png)


#### Generating a clean voltage input source

Our circuit includes components to provide a clean voltage input source.  This is necessary as, while our prototype utilizes a clean voltage supply, batteries in the commerical product might be suspect to certain quirks (e.g. voltage drops).  As our product is an analog sensor, having a constant voltage level ensures consistent performance in the field.

Our circuit does this through the use of two capacitors.  One capacitor has a higher capacitance, while the other has a lower capacitance.  This was done to anticipate both sudden and relatively more gradual changes in input voltage levels, as the responsiveness of a capacitor is given by the equation:

$$
V = V_0 \exp \left( \frac {-t} {RC} \right)
$$

Where $$$RC$$$ refers to the time constant, or rather the rate of discharge of the capacitor. 

The values chosen were $$$220nF$$$ and $$$0.68\mu F$$$ to anticipate small and relatively longer drops across the region.

This was similarly done for the 15V external supply.


#### Generating Infrared Light via the IR transmitters

4 TIL83s were connected in parallel.  Each had a resistor attached to it in series.  While it is more commercially viable to reduce costs by calculating the effective resistance and using a single resistor instead of using 4 resistors, this was not done.  Having such a set-up would result in complicating matters.  For instance, had a single LED degraded due to voltage spikes, the combined intensity of LEDs would change, resulting in the sensor being wrongly calibrated.

Multiple resistance values were tested for optimal brightness.  However, after validation through an infrared-enabled camera, the value of $$$22.1 \Omega$$$ was found to be the most suitable value for maximum brightness.  Having maximum brightness is essential, for infrared signals degrade rapidly in water, as shown by the graph in the **Background** section.


#### Detecting Infrared Light via the IR receiver

A TIL81 photodiode was used to detect Infrared light from the infrared emitters.  As the TIL81 acts as a transistor in reverse-bias mode, the TIL81 was connected as such.

The resistor in series was chosen to be $$$100k\Omega$$$.  The relatively much larger resistance as compared to the effective resistance of the infrared resistors ensures that output voltage readings are mapped to a 5V level scale.

In sum, the presence of light would correlate to a higher voltage reading, and conversely.  This reading constitutes the sensor reading, $$$v_{raw}$$$.

#### Output

Sensor output was put through an op-amp buffer, as a precautionary decoupling measure.

