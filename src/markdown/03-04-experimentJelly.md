### Gelatinous / Viscous tinted substances

Jelly crystals substituted our test for impurities in water.  We fond that while the system was able to detect the prtesence of jelly crystals from pure water, it was not as efficient in determining the amount of such crystals present.

![](plots/Jelly Crystals.png)

Pure water gave a reading of 2.318V.  However, from the graph, subsequent additions hovered between 1.768V to 1.87V.  The lack of reponsiveness in the system suggests that infrared light is not being blocked by the tinted substance - explanable as the jelly was purplish-blue in color and should have only absorbed light in the spectrum of $$$\approx 400\,nm$$$.

![](images/2014-04-08 10.43.16.jpg)