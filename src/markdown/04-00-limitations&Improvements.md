### Limitations & Improvements

Although we have exhibited promising experimental results, will still need to explore the limitations of our device.

#### Tinted solutions

Tests with Jelly crystals suggest that our sensor might not be adequate in detecting the concentration of solutions which are tinted or colorless or nature, for example Sprite and Pepsi.  This might be accounted for, as substances such as coke may be absorb light due to its specific chemical composition.

As our sensor is able to instead detect minute quantites of particles in a solution, this might however be a good instant litmus test for the presence of impure water (as compared to measuring the boiling point of water which takes time, etc.), Quality-Control checks (for instance, determining minute quantities of impurities in brand oil).


#### Large Particles

As demonstrated by the experiments, our device is very sensitive to large particles. In such cases, it will perform as a simple digital sensor distinguishing between only two possible states: completely clear & murky. This degrades the functionality of the product and limits the scope of its application. This limitation also highlights one of the potential flaws in the current setup. Since only one IR receiver is used to process the incoming radiation, it might be possible that a large chunk of material is specifical blocking the receiver area, while other portions of the liquid are completely clear. To improve the reliability of the readings, we should consider placing an array of transmitters and receivers across the container. Then data from all the components should be collated and normalized.

#### Temperature

The context of the application is very crucial to the functionality of the device. Suppose the 'shoe-box' prototype is placed outside under the IR cast of the sun. Will the experimental results be consistent with those presented in this report? Possibly not. But does this effect render the circuit response unusable? We don't know. Any object that is above absolute zero emits IR radiation, and this can be picked up by the IR receiver. But the magnitude of this response has to be experimentally verified. This can be done easily by using a water bath and a LM35 to measure the temperature and correspondingly find its relationship to the output voltage.


#### Power supply

Currently the device relies on a voltage generator to produce the required 5V and 14V. Of course, we can use a different opamp that is specifically designed for low supply voltages. However, this will reduce the resolution of the output and make it slightly more susceptible to noise. If we want to make the device truly portable, we must replace the voltage source with batteries. But all these factors depend on the requirements of the application. In some cases, a simple instrumentation amplifier might be a sufficient substitute for the current signal processing circuit (this might reduce the voltage supply requirements).

A possible remedy would be the use of a different surface-mounted variant, which would use less power, as well as additional smoothing capacitors to ensure a cleaner yet portable power supply.