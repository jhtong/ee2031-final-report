# EE2031 Mini Lab – Liquid Adulteration Sensor

##Problem Statement

## Design

#### IR Receiver/Transmitter

#### Signal Processing

## Simulation

Model with current source

## Prototype

## Characterization & Experiments

#### Trasmitter vs. Receiver

#### Different Impurities

#### Impurity Concentration

Different materials

#### Signal Processing Characterization

Compare with simulation

## Limitations & Improvements


## PRESENTATION

Name: Sniffy
Flow: Simple sensor, Main concept, Demo, macro, micro, characterization + limitations, improvements